package com.example.rakeshrav.weatherappgo.ui.main;

import com.example.rakeshrav.weatherappgo.di.PerActivity;
import com.example.rakeshrav.weatherappgo.ui.base.MvpPresenter;

@PerActivity
public interface MainMvpPresenter<V extends MainMvpView> extends MvpPresenter<V> {

    void fetchWeatherDataFromPrefs();
    void makeServerCallForecast(int days, String latLng);
}
