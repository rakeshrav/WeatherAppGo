package com.example.rakeshrav.weatherappgo.ui.main;

import com.example.rakeshrav.weatherappgo.data.network.model.forecastData.ForecastData;
import com.example.rakeshrav.weatherappgo.ui.base.MvpView;

public interface MainMvpView extends MvpView{

    void populateDataInUI(ForecastData forecastData);
    void makeServerCall(String latLng);
}
